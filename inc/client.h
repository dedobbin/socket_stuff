#pragma once

#include <stdbool.h>

#define PORT 8080
#define MAX_MSG_LEN 80

int client_connect(char* addr);
int client_send(int sockfd, char* buff, int buff_len);
int client_receive(int sockfd, char* buff, int buff_len, bool print_verbose); 
void client_test();
