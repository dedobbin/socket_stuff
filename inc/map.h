#pragma once

#include <stdbool.h>

#define TABLE_SIZE 20

typedef char*(*Callback)(char*);

typedef struct Pair {
    char* key;
    Callback value;
    struct Pair* next; //collisions
} Pair;

typedef struct Map {
    Pair* table[TABLE_SIZE];
} Map;

Map* map_create();
bool map_insert(Map* map, char* key, Callback cb);
Callback map_get(const Map* const map, char* key);
void map_destroy(Map* map);