all:
	mkdir -p build 
	gcc -g src/main.c src/map.c src/server.c src/client.c -Iinc -o program -pthread 
	mv program build

clean:
	rm -fr build