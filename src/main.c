
#include <stdio.h>
#include <pthread.h>
#include <unistd.h>
#include <string.h>
#include "server.h"
#include "client.h"

char* test_cb(char* input){
    printf("Custom operation with input: %s, will examine input..\n", input);
    
    if (strcmp(input, "param") == 0){
        return "input looking good";
    }
    return "this input is NO good";
}

void* server_thread_func(void* args){
    int rc = server_run((Map*)args);
    if (rc < 0){
        printf("Server error %d\n", rc);
    }
    pthread_exit(NULL);
}

void* client_thread_func(void* args){
    client_test();
    pthread_exit(NULL);
}

int find_arg( char* needle, int argc, char **argv){
    for (int i = 1; i < argc; i++){
        if (strcmp(argv[i], needle) == 0){
            return i;
        }
    }
    return -1;
}

int main(int argc, char **argv){
    const int SERVER_THR_INDEX = 0;
    const int CLIENT_THR_INDEX = 1;
    pthread_t threads[2] = {0};

    Map* callbacks = NULL;
    if (argc == 1 || find_arg("--server", argc, argv) > 0){
        callbacks = map_create();
        map_insert(callbacks, "test", test_cb);

        if (pthread_create(&threads[SERVER_THR_INDEX], NULL, server_thread_func, (void*)callbacks) != 0){
            fprintf(stderr, "error creating server thread\n");
            return 1;
        }
        sleep(0.2);
    }

    if (argc == 1 || find_arg("--client", argc, argv) > 0){
        pthread_t client_thread;
        if (pthread_create(&threads[CLIENT_THR_INDEX], NULL, client_thread_func, NULL)!= 0){
            fprintf(stderr, "error creating client thread\n");
            return 1;
        }
    }

    if (threads[SERVER_THR_INDEX] > 0){
        void * server_result;
        pthread_join(threads[SERVER_THR_INDEX], &server_result);
    }

    if (threads[CLIENT_THR_INDEX] > 0){
        void * client_result;
        pthread_join(threads[CLIENT_THR_INDEX], &client_result);
    }

    if (callbacks){
        map_destroy(callbacks);
    }

    return 0;
}