#include "server.h"
#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h> 
#include <pthread.h>
//#include <netdb.h>

static Map* g_callbacks; // Not very elegant but will have to do for now

int server_init()
{
    int sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd == -1){
        perror("[SERVER] failed to create socket");
        return -1;
    }
    
    int reuse = 1;
    if (setsockopt(sockfd, SOL_SOCKET, SO_REUSEPORT, &reuse, sizeof(int)) == -1){
        perror("[SERVER] failed to set option");
        return -2;
    }

    if (setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &reuse, sizeof(int)) == -1){
        perror("[SERVER] failed to set option");
        return -3;
    }

    struct sockaddr_in server_addr = {0};
    server_addr.sin_family = AF_INET;
    server_addr.sin_addr.s_addr = htonl(INADDR_ANY);
    server_addr.sin_port = htons(PORT);

    if ((bind(sockfd, (struct sockaddr*)&server_addr, sizeof(server_addr))) != 0){
        perror("[SERVER] failed to bind");
        return -4;
    }

    if ((listen(sockfd, 5))!=0){
        perror("[SERVER] Failed to listen");
        return -5;
    }

    return sockfd;
}

//TODO: support json
bool server_parse_input(const char* const input, char** out_command, char** out_arg){
    // strok will mangle input, so copy it
    char* cpy = strdup(input);

    if (cpy[strlen(cpy)-1] == '\n'){
        cpy[strlen(cpy)-1] = '\0';
    }

    *out_command = '\0';
    *out_arg = '\0'; 
    if (strchr(cpy, ':')){
        const char delimiter = ':';
        *out_command = strtok(cpy, &delimiter);
        *out_arg = strtok(NULL, &delimiter);
        if (*out_arg == NULL) {
            *out_arg = "";
        }
    } else {
        *out_command = strdup(cpy);
        *out_arg = "";
    }
        
    // printf("[SERVER] Command is: %s (%d), arg is: %s (%d)\n", 
    //     *out_command, strlen(*out_command), *out_arg, strlen(*out_arg)
    // );
    
    return true;
}

typedef struct Connection_args{
    int connfd;
} Connection_args;

void* server_interact_with_client (void* args)
{
    int connfd = ((Connection_args*)args)->connfd;
    while (true){
        //printf("[SERVER] waiting for input\n");
        char buff[MAX_MSG_LEN] = {0};
        int n = recv(connfd, buff, sizeof(buff), 0);

        if (n < 0){
            perror("[SERVER] Failed to read");
            break;
        }

        // Check if an action is associated with the input
        if (strncmp(buff, "exit", 4) == 0){
            printf("[SERVER] closing connection\n");
            close(connfd);
            break;
        } else if (strncmp(buff, "ping", 4) == 0){
            strcpy(buff, "pong");
            send(connfd, buff, MAX_MSG_LEN, 0);
        } else {
            // Check for dynamically set callback
            char* command;
            char* arg; 
            server_parse_input(buff, &command, &arg);
            Callback cb = map_get(g_callbacks, command);
            char* response = '\0';
            if (cb != NULL){
                response = cb(arg);
            } else {
                response = "no operation done";
            }
            send(connfd, response, strlen(response), 0);
        }
    }
    pthread_exit(NULL);
}

int server_run(Map* callbacks){
    g_callbacks = callbacks;
    int sockfd = server_init();
    if (sockfd < 0){
        fprintf(stderr, "[SERVER] Failed to launch");
        return -1;
    }

    while(true){
        struct sockaddr_in client_addr = {0};
        socklen_t client_address_len = sizeof(client_addr);
        printf("[SERVER] waiting for connection\n");
        int connfd = accept(sockfd, (struct sockaddr*)&client_addr, &client_address_len);

        if (connfd < 0) {
            perror("[SERVER] Failed to accept client");
            return -2;
        }
        printf("[SERVER] Got connection\n");

        pthread_t connection_thread;
        Connection_args args;
        // In theory this could case a race condition.
        //  thread is started, this loop receives another connection, so connfd is re-assigned.
        //  before the first started thread can read it. 
        args.connfd = connfd;
        pthread_create(&connection_thread, NULL, server_interact_with_client, &args);
        //TODO: this is not most clean way..
        pthread_detach(connection_thread);
    }
    close(sockfd);
    return 1;
}