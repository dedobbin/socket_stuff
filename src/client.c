#include "client.h"
#include <arpa/inet.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/time.h>
#include <errno.h>

int client_connect(char* addr)
{
    int sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd == -1){
        perror("[CLIENT] Failed to create client socket");
        return -1;
    }

    struct sockaddr_in server_addr = {0};
    server_addr.sin_family = AF_INET;
    server_addr.sin_addr.s_addr = inet_addr(addr);
    server_addr.sin_port = htons(PORT);

    struct timeval timeout;
    timeout.tv_sec = 1;
    timeout.tv_usec = 0;

    if (setsockopt(sockfd, SOL_SOCKET, SO_RCVTIMEO, &timeout, sizeof(timeout)) < 0) {
        perror("setsockopt");
        return -2;
    }

    if (connect(sockfd, (struct sockaddr*) &server_addr, sizeof(server_addr)) != 0){
        perror("[CLIENT] failed to connect");
        return -3;
    }

    return sockfd;
}

int client_send(int sockfd, char* buff, int buff_len)
{
    int res = send(sockfd, buff, buff_len, 0);
    if (res != buff_len){
        fprintf(stderr, "Client failed to send %d bytes\n", buff_len - res);
    }
    return res;
}

int client_receive(int sockfd, char* buff, int buff_len, bool print_verbose)
{
    int n = recv(sockfd, buff, buff_len, 0);
    if (n < 0){
        if (errno == EWOULDBLOCK || errno == EAGAIN){
            print_verbose ? printf("[CLIENT] Got no response\n") : 0;
        } else {
            perror("[CLIENT] recv");
        }
        return n;
    }

    if (n == 0) {
        print_verbose ? printf("[CLIENT] Server closed connection\n") : 0;
        return n;
    }

    print_verbose ? printf("[CLIENT] Got response from server: %s\n", buff) : 0;
    return n;
}

void client_test(){
    int sockfd = client_connect("127.0.0.1");

    char buff[MAX_MSG_LEN];

    while(true){
        memset(buff, 0, sizeof(buff));

        int n = 0;
        while((buff[n++] = getchar())!= '\n');

        client_send(sockfd, buff, MAX_MSG_LEN);

        memset(buff, 0, sizeof(buff));
        client_receive(sockfd, buff, MAX_MSG_LEN, true);
    }

    close(sockfd);
}